import { Injectable, CanActivate, ExecutionContext, HttpException, HttpStatus } from '@nestjs/common';
import * as jwt from 'jwt-decode';

@Injectable()
export class AdminGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const token = this.getToken(request);
    const decodeToken = this.decodeToken(token);
    return decodeToken['http://localhost:3000/roles'][0] === 'admin';
  }

  decodeToken(token) {
    try {
      const decoded = jwt(token);
      return decoded;
    } catch (err) {
      const message = 'Token error: ' + (err.message || err.name);
      throw new HttpException(message, HttpStatus.FORBIDDEN);
    }
  }

  getToken(req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
      return req.headers.authorization.split(' ')[1];
    }
    throw new HttpException('Invalid token', HttpStatus.FORBIDDEN);
  }
}
